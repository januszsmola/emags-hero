<?php

namespace App;

use App\Character\Base;

/**
 * Character Manager that keeps track of fighting characters.
 *
 * @package App
 */
class CharacterManager
{
    private $firstCharacter;
    private $secondCharacter;

    private $sortedCharacters = [];

    public function __construct(Base $firstCharacter, Base $secondCharacter)
    {
        $this->firstCharacter = $firstCharacter;
        $this->secondCharacter = $secondCharacter;

        $this->firstCharacter->setStartingStats();
        $this->secondCharacter->setStartingStats();

        $this->sortedCharacters = $this->order();
    }

    /**
     * Calculates correct order of characters based on their speed and luck.
     *
     * @return array
     */
    private function order()
    {
        $firstCharacter = $this->firstCharacter;
        $secondCharacter = $this->secondCharacter;

        if ($firstCharacter->getSpeed() > $secondCharacter->getSpeed()) {
            return [$firstCharacter, $secondCharacter];
        } elseif ($firstCharacter->getSpeed() < $secondCharacter->getSpeed()) {
            return [$secondCharacter, $firstCharacter];
        } elseif ($firstCharacter->getLuck() > $secondCharacter->getLuck()) {
            return [$firstCharacter, $secondCharacter];
        } elseif ($firstCharacter->getLuck() < $secondCharacter->getLuck()) {
            return [$secondCharacter, $firstCharacter];
        }

        // randomly select order for same speed and luck
        $order = [$firstCharacter, $secondCharacter];
        shuffle($order);

        return $order;
    }

    /**
     * Gets first character that was added
     *
     * @return Base
     */
    public function getFirst(): Base
    {
        return $this->firstCharacter;
    }

    /**
     * Gets second character that was added
     *
     * @return Base
     */
    public function getSecond(): Base
    {
        return $this->secondCharacter;
    }

    /**
     * Get faster character
     *
     * @return Base
     */
    public function getFaster(): Base
    {
        return $this->sortedCharacters[0];
    }

    /**
     * Get slower character;
     *
     * @return Base
     */
    public function getSlower(): Base
    {
        return $this->sortedCharacters[1];
    }
}