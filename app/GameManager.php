<?php
/**
 * Opis pliku
 *
 * @author: Janusz Smoła
 */

namespace App;


use \App\BattleLog\BattleLog;

class GameManager
{
    private $maxTurns = 20;
    private $gameOver = false;
    private $battleLog;
    private $characterManager;

    public function __construct(
        BattleLog $battleLog,
        CharacterManager $characterManager
    )
    {
        $this->battleLog = $battleLog;
        $this->characterManager = $characterManager;
    }

    /**
     * Calculates all the game turns at once
     */
    public function calculateAllTurns(): void
    {
        $firstCharacter = $this->characterManager->getFirst();
        $secondCharacter = $this->characterManager->getSecond();

        while (!$this->gameOver) {
            $this->calculateNextTurn();
        }

        $this->battleLog->previousTurn();

        if ($firstCharacter->getHealth() == 0) {
            $this->battleLog->addEntry(
                sprintf(
                    'Winner: %s',
                    $secondCharacter->getName()
                ),
                'alert alert-success'
            );
        } elseif ($secondCharacter->getHealth() == 0)
            $this->battleLog->addEntry(
                sprintf(
                    'Winner: %s',
                    $firstCharacter->getName()
                ),
                'alert alert-success');
        elseif ($this->battleLog->getTurn() > $this->maxTurns) {
            $this->battleLog->addEntry('Turn limit exceeded.', 'alert alert-success');
        }
    }

    /**
     * Calculate next turn
     */
    public function calculateNextTurn(): void
    {
        $this->battleLog->addEntry(
            sprintf('Turn %d begins', $this->battleLog->getTurn())
        );


        $fasterCharacter = $this->characterManager->getFaster();
        $slowerCharacter = $this->characterManager->getSlower();

        $fasterCharacter->attack($slowerCharacter);
        if ($slowerCharacter->getHealth() > 0) {
            $slowerCharacter->attack($fasterCharacter);
        }

        $this->battleLog->addEntry(sprintf(
            '%s has %d out of %d health left.',
            $this->characterManager->getFaster()->getName(),
            $this->characterManager->getFaster()->getHealth(),
            $this->characterManager->getFaster()->getMaxHealth()
        ));
        $this->battleLog->addEntry(sprintf(
            '%s has %d out of %d health left.',
            $this->characterManager->getSlower()->getName(),
            $this->characterManager->getSlower()->getHealth(),
            $this->characterManager->getSlower()->getMaxHealth()
        ));

        $this->battleLog->nextTurn();

        if (
            $this->battleLog->getTurn() > $this->maxTurns
            || $fasterCharacter->getHealth() == 0
            || $slowerCharacter->getHealth() == 0
        ) {
            $this->gameOver = true;
        }
    }

    /**
     * Checks if game is set as already over
     *
     * @return bool
     */
    public function isGameOver(): bool
    {
        return $this->gameOver;
    }

    /**
     * Add both character stats to battle log
     */
    public function logCharactersStats(): void
    {
        $this->battleLog->addEntry(
            $this->characterManager->getFirst()->getCharacterStats()
        );
        $this->battleLog->addEntry(
            $this->characterManager->getSecond()->getCharacterStats()
        );
    }


}