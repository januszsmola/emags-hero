<?php

namespace App;

use \App\Character\Base as CharacterBase;
use \App\BattleLog\BattleLog;

/**
 * Factory to create correct instances of characters.
 *
 * @package App
 */
class CharacterFactory
{
    /**
     * Creates character based on his name.
     *
     * @param string $characterName
     * @param BattleLog $battleLog
     * @return CharacterBase
     */
    public function createCharacter(string $characterName, BattleLog $battleLog): CharacterBase
    {
        $randomGenerator = new RandomGenerator();
        $className = '\App\Character\\' . $characterName;
        if (!class_exists($className) || $className == 'Base') {
            throw new \InvalidArgumentException(sprintf('There is no character named: %s', $className));
        }
        $character = new $className($randomGenerator, $battleLog);

        return $character;
    }
}