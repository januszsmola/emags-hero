<?php

namespace App;

class RandomGenerator
{
    /**
     * Get 0-100 value
     *
     * @return int
     */
    public function getPercentage()
    {
        return rand(0, 100);
    }

    /**
     * Get value between min and max
     *
     * @param $min
     * @param $max
     * @return int
     */
    public function getRandomIntFromRange($min, $max)
    {
        return rand($min, $max);
    }
}