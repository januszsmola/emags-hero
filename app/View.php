<!doctype html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>

<div class="container">
    <h1>eMAG’s Hero</h1>
    <div class="d-flex flex-row mb-3 justify-content-between">
        <div><?php echo array_shift($entriesByTurn[1])->getMessage(); ?></div>
        <div>vs</div>
        <div><?php echo array_shift($entriesByTurn[1])->getMessage(); ?></div>
    </div>

    <div class="accordion" id="turn-container">
        <?php foreach ($entriesByTurn as $turnNumber => $turnEntries): ?>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button
                                class="btn btn-link btn-block text-left"
                                type="button"
                                data-toggle="collapse"
                                data-target="#turn-<?php echo $turnNumber; ?>-container"
                                aria-expanded="<?php echo $turnNumber == 1 ? 'true' : 'false'; ?>"
                                aria-controls="turn-<?php echo $turnNumber; ?>-container">
                            Turn <?php echo $turnNumber; ?>
                        </button>
                    </h2>
                </div>
                <div class="collapse" id="turn-<?php echo $turnNumber; ?>-container" data-parent="#turn-container">
                    <div class="card-body">
                        <?php foreach ($turnEntries as $entry): ?>
                            <div class="<?php echo $entry->getCssClass(); ?>">
                                <?php echo $entry->getMessage(); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
</body>
</html>
