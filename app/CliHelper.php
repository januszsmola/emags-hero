<?php

namespace App;

use App\Character\Base;

class CliHelper
{
    private $width = 80;
    private $useWindowsCommands = false;

    public function __construct()
    {
        if (strncasecmp(PHP_OS, 'WIN', 3) == 0) {
            $this->useWindowsCommands = true;
        }
    }

    /**
     * Clear terminal screen
     */
    public function clearScreen(): void
    {
        if ($this->useWindowsCommands) {
            system('cls');
        } else {
            system('clear');
        }
    }

    /**
     * Draws a line in terminal
     */
    public function drawLine()
    {
        echo str_repeat('*', $this->width) . $this->newLine();
    }

    /**
     * Get caret to new line
     *
     * @return string
     */
    public function newLine()
    {
        return "\r\n";
    }

    /**
     * Creates borders for texts
     *
     * @param string $firstText
     * @param string $secondText
     * @param int $windowSize
     */
    public function justifyTwoWithWindow(string $firstText, string $secondText, int $windowSize)
    {
        $firstText = substr($firstText, 0, $windowSize);
        $secondText = substr($secondText, 0, $windowSize);

        echo sprintf(
                '* %s %s %s *',
                str_pad($firstText, $windowSize),
                str_repeat('*', $this->width - 6 - (2 * $windowSize)),
                str_pad($secondText, $windowSize, ' ', STR_PAD_LEFT)
            ) . $this->newLine();
    }

    /**
     * Forces user to wait for an user input
     *
     * @param string $message
     */
    public function wait($message = 'continue...')
    {
        echo sprintf('Press enter to %s %s', $message, $this->newLine());
        $handle = fopen("php://stdin", "r");
        fgets($handle);
        fclose($handle);
    }

    /**
     * Draw stats in the terminal.
     *
     * @param Base $firstCharacter
     * @param Base $secondCharacter
     */
    public function drawStats(Base $firstCharacter, Base $secondCharacter)
    {
        $statWindowSize = 17;

        $this->drawLine();

        $this->justifyTwoWithWindow(
            $firstCharacter->getName(),
            $secondCharacter->getName(),
            $statWindowSize
        );

        $this->justifyTwoWithWindow(
            '',
            '',
            $statWindowSize
        );

        $this->justifyTwoWithWindow(
            sprintf('HP: %d/%d', $firstCharacter->getHealth(), $firstCharacter->getMaxHealth()),
            sprintf('HP: %d/%d', $secondCharacter->getHealth(), $secondCharacter->getMaxHealth()),
            $statWindowSize
        );

        $this->justifyTwoWithWindow(
            sprintf('Strength: %d', $firstCharacter->getStrength()),
            sprintf('Strength: %d', $secondCharacter->getStrength()),
            $statWindowSize
        );

        $this->justifyTwoWithWindow(
            sprintf('Defence: %d', $firstCharacter->getDefence()),
            sprintf('Defence: %d', $secondCharacter->getDefence()),
            $statWindowSize
        );

        $this->justifyTwoWithWindow(
            sprintf('Speed: %d', $firstCharacter->getSpeed()),
            sprintf('Speed: %d', $secondCharacter->getSpeed()),
            $statWindowSize
        );

        $this->justifyTwoWithWindow(
            sprintf('Luck: %d', $firstCharacter->getLuck()),
            sprintf('Luck: %d', $secondCharacter->getLuck()),
            $statWindowSize
        );

        $this->justifyTwoWithWindow(
            '',
            '',
            $statWindowSize
        );

        $this->drawLine();
    }

    /**
     * Get parameters that was used to start the cli application
     *
     * @param string $optionName
     * @param $defaultValue
     * @return mixed
     */
    public function getOption(string $optionName, $defaultValue)
    {
        $cliOptions = getopt($optionName . '::');
        if (!empty($cliOptions[$optionName])) {
            return $cliOptions[$optionName];
        }

        return $defaultValue;
    }
}