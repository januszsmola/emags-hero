<?php

namespace App\Character\Skills;

/**
 * Teaches a character to do Magic Shield
 *
 * @package App\Character\Skills
 */
trait MagicShield
{
    /**
     * Calculations for Magic Shield skill
     *
     * @param int $damage
     * @return int
     */
    private function magicShield(int $damage): int
    {
        $potentialDamage = $damage - $this->defence;
        if ($potentialDamage < 0) {
            $potentialDamage = 0;
        }

        $this->battleLog->addEntry(
            sprintf(
                '%s uses Magic Shield to reduce incoming damage from %d to %d.',
                $this->name,
                $potentialDamage,
                floor(($potentialDamage) / 2)
            ),
            'alert alert-warning'
        );
        return $damage - floor(($potentialDamage) / 2);
    }
}