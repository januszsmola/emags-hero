<?php

namespace App\Character\Skills;

use App\Character\Base;

/**
 * Teaches a character to do Rapid Strike
 *
 * @package App\Character\Skills
 */
trait RapidStrike
{
    /**
     * Calculations for Rapid Strike skill, this might also go to trait if that should be included in other characters
     *
     * @param Base $target
     * @return int
     */
    private function rapidStrike(Base $target): int
    {
        $this->battleLog->addEntry(
            sprintf('%s uses Rapid Strike.', $this->name),
            'alert alert-warning'
        );
        $totalDamage = parent::attack($target);
        $totalDamage += parent::attack($target);
        return $totalDamage;
    }
}