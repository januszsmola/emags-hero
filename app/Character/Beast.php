<?php

namespace App\Character;

/**
 * Wild beasts that might be encountered in forests of Emagia
 *
 * This most basic enemy can be used as a template to create new characters.
 *
 * @package App\Character
 */
class Beast extends Base
{

    /**
     * Stat ranges that are set according to instructions
     *
     * @var array
     */
    protected $statRanges = [
        'health' => [60, 90],
        'strength' => [60, 90],
        'defence' => [40, 60],
        'speed' => [40, 60],
        'luck' => [25, 40]
    ];

    /**
     * Name for this character
     *
     * @var string
     */
    protected $name = 'Beast';
}