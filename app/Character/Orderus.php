<?php

namespace App\Character;

use App\Character\Skills\MagicShield;
use App\Character\Skills\RapidStrike;

/**
 * Hero that possesses offensive and defensive skill
 *
 * @package App\Character
 */
class Orderus extends Base
{
    use MagicShield;
    use RapidStrike;

    /**
     * Stat ranges that are set according to instructions
     *
     * @var array
     */
    protected $statRanges = [
        'health' => [70, 100],
        'strength' => [70, 80],
        'defence' => [45, 55],
        'speed' => [40, 50],
        'luck' => [10, 30]
    ];

    /**
     * Name for this character
     *
     * @var string
     */
    protected $name = 'Orderus';

    /**
     * Orderus attack action.
     *
     * It might attack normally or use Rapid Strike which strikes twice.
     *
     * @param Base $target
     * @return int
     */
    public function attack(Base $target): int
    {
        if ($this->randomGenerator->getPercentage() >= 90) {
            return $this->rapidStrike($target);
        }

        return parent::attack($target);
    }


    /**
     * Orderus defend action.
     *
     * It might defend normally or use his special Magic Shield to mitigate half of the damage.
     *
     * @param int $damage
     * @param string $attackMessage
     * @return int
     */
    public function defend(int $damage, string $attackMessage): int
    {
        if ($this->randomGenerator->getPercentage() >= 80) {
            $damage = $this->magicShield($damage);
        }
        return parent::defend($damage, $attackMessage);
    }
}