<?php

namespace App\Character;

/**
 * This extra character can help with testing this app.
 *
 * He is so lucky that he always dodge incoming attacks.
 * His speed is 0, so he will always be attacking second.
 * However his strength is small enough to never pass through Orderus defence.
 *
 * Without changing his stats fights involving him should always end in a tie.
 *
 * @package App\Character
 */
class Lakiluk extends Base
{
    /**
     *
     *
     * @var array
     */
    protected $statRanges = [
        'health' => [10000, 20000],
        'strength' => [1, 1],
        'defence' => [0, 0],
        'speed' => [0, 0],
        'luck' => [100, 100]
    ];

    protected $name = 'Lakiluk';
}