<?php

namespace App\Character;

use App\BattleLog\BattleLog;
use App\RandomGenerator;

/**
 * Base class for characters.
 *
 * All fight participants need to be instances of this class.
 *
 * @package App\Character
 */
abstract class Base
{
    /**
     * Current health, minimum 0
     *
     * @var int
     */
    protected $health;

    /**
     * Maximum health
     *
     * @var int
     */
    protected $maxHealth;

    /**
     * Current strength
     *
     * @var int
     */
    protected $strength;

    /**
     * Current defence
     *
     * @var int
     */
    protected $defence;

    /**
     * Current speed
     *
     * @var int
     */
    protected $speed;

    /**
     * Current luck
     *
     * @var int
     */
    protected $luck;

    /**
     * Random generator instance that is used to generate random values
     *
     * @var RandomGenerator
     */
    protected $randomGenerator;

    /**
     * Array of stats that are used to generate character.
     *
     * An array needs to have keys: health, strength, defence, speed, luck.
     * Values need to be arrays containing two int elements
     *
     * @var array
     */
    protected $statRanges;

    /**
     * Name of character, displayed during a fight and in battle logs
     *
     * @var string
     */
    protected $name = '';

    /**
     * Creates a character
     *
     * @param array $stats
     */
    public function __construct(RandomGenerator $randomGenerator, BattleLog $battleLog)
    {
        $this->randomGenerator = $randomGenerator;
        $this->battleLog = $battleLog;
    }

    /**
     * Gets current health
     *
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * Gets maximum health
     *
     * @return int
     */
    public function getMaxHealth(): int
    {
        return $this->maxHealth;
    }

    /**
     * Gets current speed
     *
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * Gets current strength
     *
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * Gets current defence
     *
     * @return int
     */
    public function getDefence(): int
    {
        return $this->defence;
    }

    /**
     * Gets current luck
     *
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * Takes damage to this character and returns its current health
     *
     * @param int $damage
     * @return int
     */
    public function takeDamage(int $damage): int
    {
        if ($damage < 0) {
            return $this->health;
        }

        $this->health -= $damage;

        if ($this->health < 0) {
            $this->health = 0;
        }

        return $this->health;
    }

    /**
     * Sets all of the required stats for a character
     */
    public function setStartingStats(): void
    {
        $stats = $this->generateStats();
        $requiredFields = ['health', 'strength', 'defence', 'speed', 'luck'];

        foreach ($requiredFields as $fieldName) {
            if (!isset($stats[$fieldName])) {
                throw new \InvalidArgumentException(sprintf('Missing stat: %s', $fieldName));
            }

            $this->$fieldName = (int)$stats[$fieldName];
        }
        $this->maxHealth = $this->health;
    }

    /**
     * Determines if action is lucky
     */
    public function isLucky()
    {
        return $this->luck == 100 || $this->randomGenerator->getPercentage() > 100 - $this->luck;
    }

    /**
     * Does calculations to defend action
     *
     * @param int $damage
     * @return int total damage done to defender
     */
    public function defend(int $damage, string $attackMessage): int
    {
        if ($this->isLucky()) {
            $this->battleLog->addEntry(
                sprintf('%s but the attack misses.', $attackMessage),
                'alert alert-info'
            );
            return 0;
        }

        $damageDone = $damage - $this->defence;
        if ($damageDone < 0) {
            $this->battleLog->addEntry(
                sprintf(
                    '%s but attack is too weak to deal any damage.',
                    $attackMessage
                ),
                'alert alert-info'
            );
        } else {
            $this->takeDamage($damageDone);
            $this->battleLog->addEntry(
                sprintf(
                    '%s and deals %d damage to %s.',
                    $attackMessage,
                    $damage - $this->defence,
                    $this->getName()
                ),
                'alert alert-danger'
            );
        }

        return $damage;
    }

    /**
     * Does calculations to attack action
     *
     * @return int
     */
    public function attack(Base $target): int
    {
        $attackMessage = sprintf('%s attacks', $this->getName());
        return $target->defend($this->strength, $attackMessage);
    }

    /**
     * Generates character stats
     *
     * @return array
     */
    protected function generateStats(): array
    {
        $generatedStats = [];
        foreach ($this->statRanges as $statName => $range) {
            $generatedStats[$statName] = $this->randomGenerator->getRandomIntFromRange($range[0], $range[1]);
        }

        return $generatedStats;
    }

    /**
     * Gets name given to the character. It doesn't need to be the same as class name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Merges all of character stats into one string
     *
     * @return string
     */
    public function getCharacterStats(): string
    {
        return sprintf(
            '%s starts with HP: %d, Str: %d, Def: %d, Spd: %d, Lck: %d',
            $this->getName(),
            $this->getHealth(),
            $this->getStrength(),
            $this->getDefence(),
            $this->getSpeed(),
            $this->getLuck()
        );
    }
}