<?php

namespace App\BattleLog;

/**
 * Battlelog that enables managers and characters to log progress of battle.
 *
 * @package App/BattleLog
 */
class BattleLog
{
    /**
     * List of all entries to battle log.
     *
     * @var array
     */
    private $entries = [];

    /**
     * Track of last displayed entries
     *
     * @var ?int
     */
    private $lastDisplayedEntry = null;

    /**
     * Track of turns
     *
     * @var int
     */
    private $currentTurn = 1;

    /**
     * Adds new entry to battle log
     *
     * @param string $entry
     * @return array
     */
    public function addEntry(string $message, string $cssClass = 'alert alert-dark'): array
    {
        $this->entries[] = new Entry(
            $message,
            $this->currentTurn,
            $cssClass
        );
        return $this->entries;
    }

    /**
     * Get current turn number
     *
     * @return int
     */
    public function getTurn(): int
    {
        return $this->currentTurn;
    }

    /**
     * Sets current turn counter to next number
     */
    public function nextTurn(): void
    {
        $this->currentTurn += 1;
    }

    /**
     * Sets current turn counter to previous number
     */
    public function previousTurn(): void
    {
        $this->currentTurn -= 1;
    }

    /**
     * Returns array of all entries in battle log. Sets last displayed entry to array length.
     *
     * @return array
     */
    public function getAllEntries(): array
    {
        $this->lastDisplayedEntry = count($this->entries);
        return $this->entries;
    }

    /**
     * Returns array of entries added since last method that get entries from this battle log.
     *
     * @return array
     */
    public function getEntriesSinceLastGet(): array
    {
        $slicedArray = array_slice($this->entries, $this->lastDisplayedEntry);
        $this->lastDisplayedEntry = count($this->entries);
        return $slicedArray;
    }

    /**
     * Returns array of all entries in battle log grouped by turn. Sets last displayed entry to array length.
     *
     * @return array
     */
    public function getEntriesByTurn(): array
    {
        $entriesGrouped = [];
        foreach ($this->entries as $entry) {
            $entriesGrouped[$entry->getTurn()][] = $entry;
        }
        return $entriesGrouped;
    }
}