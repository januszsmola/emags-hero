<?php

namespace App\BattleLog;

/**
 * Entry for battlelog
 *
 * @package App\BattleLog
 */
class Entry
{
    /**
     * Entry message
     *
     * @var string
     */
    private $message;

    /**
     * CSS class for entry
     *
     * @var string
     */
    private $cssClass;

    /**
     * Turn that entry belongs to
     *
     * @var int
     */
    private $turn;

    /**
     * Creates entry with given message and CSS class
     * @param string $message
     * @param string $cssClass
     */
    public function __construct(string $message, int $turn, string $cssClass = '')
    {
        $this->turn = $turn;
        $this->message = $message;
        $this->cssClass = $cssClass;
    }

    /**
     * Get message of entry
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Get css class of entry
     *
     * @return string
     */
    public function getCssClass(): string
    {
        return $this->cssClass;
    }

    /**
     * Get turn number when this entry was added
     *
     * @return string
     */
    public function getTurn(): string
    {
        return $this->turn;
    }
}