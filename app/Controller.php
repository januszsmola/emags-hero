<?php

namespace App;

use App\Character\Base;
use App\BattleLog\BattleLog;

class Controller
{
    /**
     * @var GameManager
     */
    private $gameManager;

    /**
     * @var BattleLog
     */
    private $battleLog;

    /**
     * @var CharacterManager
     */
    private $characterManager;

    /**
     * @var CharacterFactory
     */
    private $characterFactory;

    public function __construct()
    {
        $this->battleLog = new BattleLog();
        $this->characterFactory = new CharacterFactory();
    }

    /**
     * Runs the application and display browser-friendly output
     */
    public function web(): void
    {
        if (!empty($_GET['f'])) {
            $firstCharacterName = $_GET['f'];
        } else {
            $firstCharacterName = 'Orderus';
        }

        if (!empty($_GET['s'])) {
            $secondCharacterName = $_GET['s'];
        } else {
            $secondCharacterName = 'Beast';
        }
        $this->instantiateCharacterManager($firstCharacterName, $secondCharacterName);
        $this->instantiateGameManager();

        $this->characterManager = new CharacterManager(
            $this->characterFactory->createCharacter('Orderus', $this->battleLog),
            $this->characterFactory->createCharacter('Beast', $this->battleLog)
        );
        $this->gameManager->logCharactersStats();
        $this->gameManager->calculateAllTurns();

        $entriesByTurn = $this->battleLog->getEntriesByTurn();

        require(__DIR__ . '/View.php');
    }

    /**
     * Runs the application and returns cli friendly output
     */
    public function cli(): void
    {
        $cli = new CliHelper();
        $cli->clearScreen();

        $firstCharacterName = $cli->getOption('f', 'Orderus');
        $secondCharacterName = $cli->getOption('s', 'Beast');

        $this->instantiateCharacterManager($firstCharacterName, $secondCharacterName);
        $this->instantiateGameManager();

        $firstCharacter = $this->characterManager->getFirst();
        $secondCharacter = $this->characterManager->getSecond();

        $cli->drawStats($firstCharacter, $secondCharacter);
        $cli->wait('begin fight');

        while (!$this->gameManager->isGameOver()) {
            $this->processCliTurn($cli, $firstCharacter, $secondCharacter);
        }
    }

    /**
     * Processes single turn in cli
     *
     * @param CliHelper $cli
     * @param Base $firstCharacter
     * @param Base $secondCharacter
     */
    private function processCliTurn(CliHelper $cli, Base $firstCharacter, Base $secondCharacter)
    {
        $this->gameManager->calculateNextTurn();
        $cli->clearScreen();
        $cli->drawStats($firstCharacter, $secondCharacter);
        foreach ($this->battleLog->getEntriesSinceLastGet() as $entry) {
            echo $entry->getMessage() . $cli->newLine();
        }

        if (!$this->gameManager->isGameOver()) {
            $cli->wait('next turn');
        } else {
            if ($firstCharacter->getHealth() == 0) {
                echo sprintf('%s wins!', $secondCharacter->getName()) . $cli->newLine();
            } elseif ($secondCharacter->getHealth() == 0) {
                echo sprintf('%s wins!', $firstCharacter->getName()) . $cli->newLine();
            } else {
                echo 'Draw!' . $cli->newLine();
            }
            $cli->wait('exit');
        }
    }

    /**
     * Creates instance of character manager
     *
     * @param $firstCharacterName
     * @param $secondCharacterName
     */
    private function instantiateCharacterManager($firstCharacterName, $secondCharacterName)
    {
        $this->characterManager = new CharacterManager(
            $this->characterFactory->createCharacter($firstCharacterName, $this->battleLog),
            $this->characterFactory->createCharacter($secondCharacterName, $this->battleLog)
        );
    }

    /**
     * Creates instance of game manager
     */
    private function instantiateGameManager()
    {
        $this->gameManager = new GameManager(
            $this->battleLog,
            $this->characterManager
        );
    }
}