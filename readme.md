# eMAG’s Hero

Fight simulator between two characters with several stats and skills

## Installation

Use the composer to install necessary packages and generate autoload

```bash
composer install
```

## Usage

### CLI
To simulate fight just run:
```bash
php bin/app.php
```
CLI version waits for user input, so you need to press enter every time you want to progress to the next turn.

You can also run it with extra parameters to specify first `-f` and second `-s` fighting characters. 
For example if you want to simulate mirror match of two Beasts then you can run:
```bash
php bin/app.php -f=Beast -s=Beast
```

### Web
To simulate a fight you need to set up a web server and run public/index.php file.

Web version runs all turns at once so they can be viewed in any order.

This can be run with extra parameters to specify first `f` and second `s` parameters in the query string. 
For example if you want to simulate mirror match of two Orderuses then you can run:
```bash
index.php?f=Orderus&s=Orderus
```

