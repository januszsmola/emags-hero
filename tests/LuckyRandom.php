<?php

namespace Tests;

use \App\RandomGenerator;

class LuckyRandom extends RandomGenerator
{
    public function getPercentage()
    {
        return 100;
    }

    public function getRandomIntFromRange($min, $max)
    {
        return $max;
    }
}