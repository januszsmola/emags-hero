<?php

namespace Tests\Character;

use App\BattleLog\BattleLog;
use App\Character\Orderus;
use App\RandomGenerator;
use PHPUnit\Framework\TestCase;
use Tests\LuckyRandom;
use Tests\UnluckyRandom;

class OrderusTest extends TestCase
{
    use MockedBaseTrait;

    private $statsMock = [
        'health' => 1000,
        'strength' => 100,
        'defence' => 30,
        'speed' => 50,
        'luck' => 50
    ];

    private $randomGenerator;
    private $battleLog;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $this->randomGenerator = new RandomGenerator();
        $this->battleLog = new BattleLog();
        parent::__construct($name, $data, $dataName);
    }

    public function testAttacks()
    {
        $attacker = new Orderus($this->randomGenerator, $this->battleLog);
        $defender = $this->getMockedBase(
            $this->statsMock,
            new UnluckyRandom(),
            $this->battleLog
        );
        $attacker->setStartingStats();
        $defender->setStartingStats();

        $defenderStartingHealth = $defender->getHealth();

        // 1st attack
        $attacker->attack($defender);
        $defenderHealthAfterFirstAttack = $defender->getHealth();
        $this->assertLessThan($defenderStartingHealth, $defenderHealthAfterFirstAttack);

        // 2nd attack
        $attacker->attack($defender);
        $this->assertLessThan($defenderStartingHealth, $defender->getHealth());
        $this->assertLessThan($defenderHealthAfterFirstAttack, $defender->getHealth());
    }

    public function testBasicAttacks()
    {
        $attacker = new Orderus(new UnluckyRandom(), $this->battleLog);
        $defender = $this->getMockedBase(
            $this->statsMock,
            new UnluckyRandom(),
            $this->battleLog
        );

        $attacker->setStartingStats();
        $defender->setStartingStats();

        $defenderStartingHealth = $defender->getHealth();

        // 1st attack
        $attacker->attack($defender);
        $this->assertEquals(
            $defenderStartingHealth - $attacker->getStrength() + $defender->getDefence(),
            $defender->getHealth()
        );
        $healthAfterFirstAttack = $defender->getHealth();

        // 2nd attack
        $attacker->attack($defender);
        $this->assertEquals(
            $healthAfterFirstAttack - $attacker->getStrength() + $defender->getDefence(),
            $defender->getHealth()
        );
    }

    public function testSpecialAttack()
    {
        $attacker = new Orderus(new LuckyRandom(), $this->battleLog);
        $defender = $this->getMockedBase(
            $this->statsMock,
            new UnluckyRandom(),
            $this->battleLog
        );

        $attacker->setStartingStats();
        $defender->setStartingStats();

        $defenderStartingHealth = $defender->getHealth();

        $attacker->attack($defender);
        $this->assertEquals(
            $defenderStartingHealth - 2 * ($attacker->getStrength() - $defender->getDefence()),
            $defender->getHealth()
        );
        $healthAfterFirstAttack = $defender->getHealth();

        // 2nd attack
        $attacker->attack($defender);
        $this->assertEquals(
            $healthAfterFirstAttack - 2 * ($attacker->getStrength() - $defender->getDefence()),
            $defender->getHealth()
        );
    }
}
