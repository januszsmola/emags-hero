<?php

namespace Tests\Character;

use App\BattleLog\BattleLog;
use App\RandomGenerator;
use PHPUnit\Framework\TestCase;

class BaseTest extends TestCase
{
    use MockedBaseTrait;

    private $statsMock = [
        'health' => 100,
        'strength' => 80,
        'defence' => 55,
        'speed' => 50,
        'luck' => 30
    ];

    private $randomGenerator;
    private $battleLog;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $this->randomGenerator = new RandomGenerator();
        $this->battleLog = new BattleLog();
        parent::__construct($name, $data, $dataName);
    }

    public function testMissingStartingStats()
    {
        $error = null;

        try {
            $base = $this->getMockedBase([], $this->randomGenerator, $this->battleLog);
            $base->setStartingStats();
            $this->fail();
        } catch (\InvalidArgumentException $error) {
            // $error is already set
        }
        $this->assertInstanceOf(\InvalidArgumentException::class, $error);

        try {
            $this->getMockedBase($this->statsMock, $this->randomGenerator, $this->battleLog);
        } catch (\Exception $error) {
            $this->fail();
        }
    }

    public function testDealDamage()
    {
        $attacks = [10, 20, $this->statsMock['health']];
        $base = $this->getMockedBase($this->statsMock, $this->randomGenerator, $this->battleLog);
        $base->setStartingStats();

        $this->assertEquals($this->statsMock['health'], $base->getHealth());

        $this->assertEquals(
            $this->statsMock['health'] - $attacks[0],
            $base->takeDamage($attacks[0])
        );
        $this->assertEquals(
            $this->statsMock['health'] - $attacks[0] - $attacks[1],
            $base->takeDamage($attacks[1])
        );
        $this->assertEquals(
            0,
            $base->takeDamage($attacks[2])
        );
    }
}
