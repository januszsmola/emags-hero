<?php

namespace Tests\Character;

use App\BattleLog\BattleLog;
use App\RandomGenerator;
use App\Character\Base as BaseCharacter;

trait MockedBaseTrait
{
    private function getMockedBase(array $stats, RandomGenerator $randomGenerator, BattleLog $battleLog): BaseCharacter
    {
        $base = $this->getMockBuilder(BaseCharacter::class)
            ->setMethods(['generateStats', 'getName'])
            ->setConstructorArgs([$randomGenerator, $battleLog])
            ->getMockForAbstractClass();

        $base->method('generateStats')
            ->willReturn($stats);

        $base->method('getName')
            ->willReturn('PHPUnitMock');

        return $base;
    }

}