<?php

namespace Tests;

use App\BattleLog\BattleLog;
use App\Character\Base;
use App\Character\Orderus;
use App\Character\Beast;
use App\CharacterFactory;

class CharacterFactoryTest extends \PHPUnit\Framework\TestCase
{
    public function testCharacterCreation()
    {
        $battleLog = new BattleLog();
        $characterFactory = new CharacterFactory();

        try {
            $orderus = $characterFactory->createCharacter('Orderus', $battleLog);
            $this->assertInstanceOf(Orderus::class, $orderus);
            $this->assertInstanceOf(Base::class, $orderus);
            $this->assertNotInstanceOf(Beast::class, $orderus);
        } catch (\Exception $e) {
            $this->fail(sprintf('There was problems with creating Orderus. %s', $e->getMessage()));
        }

        try {
            $beast = $characterFactory->createCharacter('Beast', $battleLog);
            $this->assertInstanceOf(Beast::class, $beast);
            $this->assertInstanceOf(Base::class, $beast);
            $this->assertNotInstanceOf(Orderus::class, $beast);
        } catch (\Exception $e) {
            $this->fail(sprintf('There was problems with creating Beast. %s', $e->getMessage()));
        }

        $nonexistingCharacterError = null;
        try {
            $this->assertFalse(class_exists('\App\Character\SomeNameThatDoesntExists'));
            $nonexistingCharacter = $characterFactory->createCharacter(
                'SomeNameThatDoesntExists',
                $battleLog
            );
            $this->fail();
        } catch (\InvalidArgumentException $nonexistingCharacterError) {
            // $nonexistingCharacterError is already set
        }

        $this->assertInstanceOf(\InvalidArgumentException::class, $nonexistingCharacterError);
    }
}
