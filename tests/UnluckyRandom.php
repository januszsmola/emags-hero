<?php

namespace Tests;

use \App\RandomGenerator;

class UnluckyRandom extends RandomGenerator
{
    public function getPercentage()
    {
        return 0;
    }

    public function getRandomIntFromRange($min, $max)
    {
        return $min;
    }
}