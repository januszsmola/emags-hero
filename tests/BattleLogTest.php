<?php

use App\BattleLog\BattleLog;
use PHPUnit\Framework\TestCase;

class BattleLogTest extends TestCase
{
    public function testAddEntry()
    {
        $battleLog = new BattleLog();
        $entries = $battleLog->addEntry('foo');
        $this->assertArrayHasKey(0, $entries);
        $this->assertArrayNotHasKey(1, $entries);
        $this->assertEquals('foo', $entries[0]->getMessage());

        $entries = $battleLog->addEntry('bar', 'test');
        $this->assertArrayHasKey(1, $entries);
        $this->assertArrayNotHasKey(2, $entries);
        $this->assertEquals('foo', $entries[0]->getMessage());
        $this->assertEquals('bar', $entries[1]->getMessage());
        $this->assertEquals('alert alert-dark', $entries[0]->getCssClass());
        $this->assertEquals('test', $entries[1]->getCssClass());
    }

    public function testGetAllEntries()
    {
        $battleLog = new BattleLog();

        $entries = ['foo', 'bar', 'baz'];
        foreach ($entries as $entry) {
            $battleLog->addEntry($entry);
        }

        $battleLogEntries = $battleLog->getAllEntries();
        $this->assertEquals(count($entries), count($battleLogEntries));
        foreach ($battleLogEntries as $key => $battleLogEntry) {
            $this->assertEquals($battleLogEntry->getMessage(), $entries[$key]);
        }

        $entries[] = 'qux';
        $battleLog->addEntry('qux');

        $battleLogEntries = $battleLog->getAllEntries();
        $this->assertEquals(count($entries), count($battleLogEntries));
        foreach ($battleLogEntries as $key => $battleLogEntry) {
            $this->assertEquals($battleLogEntry->getMessage(), $entries[$key]);
        }
    }

    public function testGetEntriesSinceLastGet()
    {
        $battleLog = new BattleLog();

        $entries = ['foo', 'bar', 'baz'];
        $newEntries = ['qux', 'quux'];
        foreach ($entries as $entry) {
            $battleLog->addEntry($entry);
        }

        $battleLog->getAllEntries();

        foreach ($newEntries as $entry) {
            $battleLog->addEntry($entry);
        }

        $entriesSinceLast = $battleLog->getEntriesSinceLastGet();
        $this->assertNotEquals(array_merge($entries, $newEntries), $entriesSinceLast);
        $this->assertEquals(count($newEntries), count($entriesSinceLast));
        foreach ($newEntries as $key => $entry) {
            $this->assertEquals($entry, $entriesSinceLast[$key]->getMessage());
        }
        $this->assertEquals([], $battleLog->getEntriesSinceLastGet());
    }
//

}
